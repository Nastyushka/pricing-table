## Завдання

Зверстати два макета [Pricing table](https://www.figma.com/file/eIsl45E8coX6WsWbKb5DtM/Pricing-Table?node-id=0%3A1).

#### Технічні вимоги до верстки

- Верстка має бути зроблена у вигляді таблиці (тег `<table>`)

#### Примітка
- Верстка повинна бути виконана без використання бібліотек CSS типу Bootstrap або Materialize.
